angular.module('starter.controllers', [])

.controller('MapCtrl', function($scope, $ionicLoading, $timeout) {

  $scope.mapCreated = function(map) {
    $scope.map = map;
  };

  window.stopper = 0;
  $scope.tracking1 = 'color:#444';
  $scope.tracking2 = 'color:red';

  $scope.trackMe = function() {
    var turnOn = 'color:red';
    var turnOff = 'color:#444';

    window.stopper = 0;
    $scope.tracking1 = turnOn;
    $scope.tracking2 = turnOff;
    showMap(1);
  }

  $scope.notrackMe = function() {
    var turnOn = 'color:red';
    var turnOff = 'color:#444';

    $scope.tracking2 = turnOn;
    $scope.tracking1 = turnOff;
    showMap(2);
  }

  $scope.centerOnMe = function() {
    if (!$scope.map) {
      return;
    } else {
      $scope.loading = $ionicLoading.show({
        content: 'Getting current location...',
        showBackdrop: false
      });

      showMap(0);
    }
  };

  function showMap(trackMe) {

    if (trackMe < 2) {

      var geoSettings = {
        frequency: 1000,
        timeout: 30000,
        enableHighAccuracy: false // may cause errors if true
      };

      navigator.geolocation.getCurrentPosition(function(pos) {

        $scope.map.setCenter(new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude));

        initialLocation = new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude);

        //remove marker.
        //markers[markers.length-1].setMap(null);

        if (trackMe == 0) {
          currentPosMarker = new google.maps.Marker({
            position: initialLocation,
            animation: google.maps.Animation.DROP,
            optimized: false,
            icon: 'img/marker.gif',
            map: $scope.map
          });
          dest = new google.maps.LatLng(51.350946, -1.994555);
          destPosMarker = new google.maps.Marker({
            position: dest,
            animation: google.maps.Animation.DROP,
            optimized: false,
            icon: 'img/roll.png',
            map: $scope.map
          });

          if(typeof $scope.loading != 'undefined')$scope.loading.hide();
        } else {
          currentPosMarker = new google.maps.Marker({
            position: initialLocation,
            //animation: google.maps.Animation.DROP,
            optimized: false,
            icon: 'img/marker.gif',
            map: $scope.map
          });
        }
      }, function(error) {
        alert('Unable to get location: ' + error.message);
      });

      if (trackMe == 1) {
        var theTimer = $timeout(function() {
          if (window.stopper != 1) showMap(1);

        }, 3000);
      }

    }
    if (trackMe == 2) {
      currentPosMarker.setMap(null);
      $timeout.cancel(theTimer);
      window.stopper = 1;
    }
  }
  showMap(0);
});
